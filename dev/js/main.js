/* frameworks */
//=include ../../node_modules/cropperjs/dist/cropper.min.js
//=include ../../node_modules/jquery-cropper/dist/jquery-cropper.min.js
//=include ../../node_modules/dom-to-image/dist/dom-to-image.min.js


/* libs */
//=include lib/modernizr-custom.js

/* plugins */
//=include plugins/wow/wow.min.js
//=include plugins/odometer.min.js
//=include plugins/jquery.viewportchecker.min.js

/* separate */
//=include helpers/object-fit.js
//=include separate/global.js

/* components */
//=include components/js-header.js
//=include components/avatar.js
//=include components/js-certificate.js

// the main code


//  Wow

new WOW().init();

// Odometer

if ($('.c-promo-car').length > 0) {
    var el = document.querySelector('.odometer');
    var odometerFromValue = 100001;
    var od;

    function odometer() {

        od = new Odometer({
            el: el,
            value: odometerFromValue,
            format: 'ddd',
            theme: 'car',
            duration: 3500,
        });

    }
    odometer();

    $('.c-promo-car').viewportChecker({
        callbackFunction: function() {
            od.update(odometerFromValue + $(el).data('value') * 10);
        },
    });
}





// Scroll to next section Promo

$("#js-scroll").click(function() {
    $([document.documentElement, document.body]).animate({
        scrollTop: $(".c-promo-congratulations").offset().top
    }, 2000);
});











// Modal ava








// //Modal certificate
// $('.share').on('click', function() {
//     $(".c-certificate__modal").addClass("active");
//     $('body').addClass("fixed-overlay");
//     $('.tab-content.current').clone().appendTo(".c-certificate__modal-body");
// });


// $('.close-modal').on('click', function() {
//     $(".c-certificate__modal").removeClass("active");
//     $('body').removeClass("fixed-overlay");
//     $(".c-certificate__modal-body .tab-content.current").remove();
// });


// jQuery(function($) {
//     $(document).mouseup(function(e) { // отслеживаем событие клика по веб-документу
//         var block = $(".c-avatar__modal-inner"); // определяем элемент, к которому будем применять условия (можем указывать ID, класс либо любой другой идентификатор элемента)
//         if (!block.is(e.target) // проверка условия если клик был не по нашему блоку
//             &&
//             block.has(e.target).length === 0) { // проверка условия если клик не по его дочерним элементам
//             $(".c-certificate__modal").removeClass("active");
//             $('body').removeClass("fixed-overlay");
//             $(".c-certificate__modal-body .tab-content.current").remove();
//         }
//     });
// });

$('.c-avatar__edit-tab-wrapper').on('click', 'ul.tabs li', function() {
    var tab_id = $(this).attr('data-tab');

    $('ul.tabs li').removeClass('current');
    $('.tab-content').removeClass('current');

    $(this).addClass('current');
    $("#" + tab_id).addClass('current');

});


// Animations

$(window).scroll(function() {
    $('.c-promo-hero , .c-promo-congratulations, .c-promo-interactive, .c-promo-car, .c-avatar, .c-stickerpack, .c-certificate, .c-global-board').each(function() {
        if ($(this).offset().top < $(window).scrollTop()) {
            var difference = $(window).scrollTop() - $(this).offset().top;
            var
                half = (difference / 3) + 'px',
                deg = (difference / 10),
                transform = 'translate3d( 0, ' + '-' + half + ',0)',
                rotate = 'rotate(' + deg + 'deg)';
            $(this).find('.parallax').css('transform', transform),
                $(this).find('.parallax-rotate').css('transform', rotate);
        } else {
            $(this).find('.parallax').css('transform', 'translate3d(0,0,0)');
            $(this).find('.parallax-rotate').css('rotate', '0deg');
        }
    });
});

//Select

$('select').each(function() {
    var $this = $(this),
        numberOfOptions = $(this).children('option').length;

    $this.addClass('select-hidden');
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());

    var $listWrapper = $('<div />', {
        'class': 'select-options-wrapper'
    }).insertAfter($styledSelect);

    var $list = $('<ul />', {
        'class': 'select-options'
    }).appendTo($listWrapper);

    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
        }).appendTo($list);
    }

    var $listItems = $list.children('li');


    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function() {
            $(this).removeClass('active').next('.select-options-wrapper').hide();
        });
        $(this).toggleClass('active').next('.select-options-wrapper').toggle();
    });

    $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $listWrapper.hide();
    });

    $(document).click(function() {
        $styledSelect.removeClass('active');
        $listWrapper.hide();
    });

});


//user modal


$('.c-user--modal').on('click', function() {
    var $this = $(this);
    var imgSrc = $(this).find('.c-global-board__user-img img').attr('src');
    var imgName = $(this).find('.c-global-board__user-name').clone().appendTo('.c-percent-board__modal-user-surname');
    console.log(imgSrc);
    console.log(imgName);
    $(".c-percent-board__modal-user-img-inner img").attr('src', imgSrc);

    $(".c-percent-board__modal-wrapper").addClass("active");
    $('body').addClass("fixed-overlay");
});


$('.c-percent-board__modal-header-close .close-modal').on('click', function() {
    $(".c-percent-board__modal-wrapper").removeClass("active");
    $('body').removeClass("fixed-overlay");
    $('.c-percent-board__modal-user-surname').find('.c-global-board__user-name').remove();
});


$('.c-user--modal-promo').on('click', function() {
    var $this = $(this);
    var imgSrc = $(this).find('.c-promo-congratulations__user-img img').attr('src');
    var imgName = $(this).find('.c-promo-congratulations__user-info').clone().appendTo('.c-percent-board__modal-user-surname');
    console.log(imgSrc);
    console.log(imgName);
    $(".c-percent-board__modal-user-img-inner img").attr('src', imgSrc);

    $(".c-percent-board__modal-wrapper").addClass("active");
    $('body').addClass("fixed-overlay");
});


$('.c-percent-board__modal-header-close .close-modal').on('click', function() {
    $(".c-percent-board__modal-wrapper").removeClass("active");
    $('body').removeClass("fixed-overlay");
    $('.c-percent-board__modal-user-surname').find('.c-promo-congratulations__user-info').remove();
});



jQuery(function($) {
    $(document).mouseup(function(e) { // отслеживаем событие клика по веб-документу
        var block = $(".c-percent-board__modal"); // определяем элемент, к которому будем применять условия (можем указывать ID, класс либо любой другой идентификатор элемента)
        if (!block.is(e.target) // проверка условия если клик был не по нашему блоку
            &&
            block.has(e.target).length === 0) { // проверка условия если клик не по его дочерним элементам
            $(".c-percent-board__modal-wrapper").removeClass("active");
            $('body').removeClass("fixed-overlay");
            $('.c-percent-board__modal-user-surname').find('.c-global-board__user-name , .c-promo-congratulations__user-info ').remove();
        }
    });
});


$('.youtube').click(function() {
    document.getElementById('video').play();
    $('.video').addClass('opened');
})

$('.close-btn').click(function() {
    document.getElementById('video').pause();
    $('.video').removeClass('opened');
})



$('.slider-main').on('afterChange', function(slick, slide) {
    capture($image);
});