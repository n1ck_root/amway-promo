$(document).ready(function() {
    // if modalWindow is visible - make page noscrollable 
    var scrlTop;
    $(window).on('scroll', function() {
        scrlTop = `${window.scrollY}px`;
    });
    var showDialog = function() {
        var body = document.body;
        body.style.position = 'fixed';
        body.style.width = '100%';
        body.style.top = `-${scrlTop}`;
    };
    var closeDialog = function() {
        var body = document.body;
        var scrollY = body.style.top;
        body.style.position = '';
        body.style.top = '';
        body.style.width = '';
        window.scrollTo(0, parseInt(scrollY || '0') * -1);
    }

    // Desktop download certificate
    $('.prepare-sert').on('click', function() {
        var $this = $(this);
        $this.addClass('loading');
        domtoimage.toPng(document.getElementById('sertificate-inner'))
            .then(function(dataUrl) {
                var link = $('.ready-sert');
                link.attr('download', 'my-certificate.png');
                link.attr('href', dataUrl);
            });
        setTimeout(function() {
            $this.hide();
            $this.removeClass('loading');
            $('.ready-sert').show();
        }, 3000);
    });

    $('.c-certificate__pick-body').on('click', 'ul.tabs li', function() {
        var tab_id = $(this).attr('data-tab');

        if ($(window).width() > 767) {
            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');

            $(this).addClass('current');
            $("#" + tab_id).addClass('current');

            $('#download-prepare').show();
            $('#download-sert').hide();
        }
    });


    // Mobile download certificate
    $('.c-certificate__pick-tab-wrapper').on('click', '.tab-link', function() {
        if ($(window).width() < 768) {
            var clonedCertificate = $(this).clone();
            var certModal = $('.c-certificate__modal');
            certModal.find('#sertificate-inner-mobile').append(clonedCertificate);
            showDialog();
            certModal.addClass('active');
        }
    });

    $('#download-prepare-mobile').on('click', function() {
        var $this = $(this);
        $this.addClass('loading');
        domtoimage.toPng(document.getElementById('sertificate-inner-mobile'))
            .then(function(dataUrl) {
                var link = $('#download-sert-mobile');
                link.attr('download', 'my-certificate.png');
                link.attr('href', dataUrl);
            });
        setTimeout(function() {
            $this.hide();
            $this.removeClass('loading');
            $('#download-sert-mobile').show();
        }, 3000);
    });

    $('.c-certificate__modal').on('click', '.close-modal', function() {
        closeDialog();
        $('.c-certificate__modal').removeClass('active');
        $('.c-certificate__modal-body').find('#sertificate-inner-mobile').html('');
        $('#download-prepare-mobile').show();
        $('#download-sert-mobile').hide();
    });

});