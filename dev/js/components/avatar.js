// Image Cropper

// Ava slider

var mainAvaSlickOptions = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: false,
    fade: true,
    asNavFor: '.slider-nav',
    swipe: false,
    infinite: false,
}

var mainNavAvaSlickOptions = {
    slidesToShow: 6,
    slidesToScroll: 1,
    asNavFor: '.slider-main',
    dots: false,
    arrows: false,
    focusOnSelect: true,
    swipe: false,
    responsive: [{
        breakpoint: 769,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
            arrows: true,
            centerMode: true
        }
    }]
}

$('.slider-main').slick(mainAvaSlickOptions);
$('.slider-nav').slick(mainNavAvaSlickOptions);


function capture($image) {

    var imgBorder = $('.slick-active img').attr("src");
    var canvas = document.getElementById("user-img");
    var ctx = canvas.getContext("2d");

    var imgMain = $image.cropper('getCroppedCanvas').toDataURL();
    var img1 = loadImage(imgBorder, main);
    //Second canvas data
    var img2 = loadImage(imgMain, main);

    var imagesLoaded = 0;
    canvas.width = 400;
    canvas.height = 400;

    function main() {
        imagesLoaded += 1;

        if (imagesLoaded == 2) {
            // composite now
            ctx.drawImage(img2, 0, 0, 400, 400);
            ctx.drawImage(img1, 0, 0, 400, 400);
        }
    }

    function loadImage(src, onload) {
        var img = new Image();
        img.onload = onload;
        img.src = src;
        return img;
    }
}

var $image = $('#profile-img');

$image.cropper({
    aspectRatio: 1 / 1,
    preview: '.preview',
    move: true,
});

var $cropper = $image.data('cropper');




//Upload photo
if ($('.c-avatar__edit').length > 0) {
    var $profileImgDiv = document.getElementById("profile-img-div"),
        $profileImg = document.getElementById("profile-img"),
        $changePhoto = document.getElementById("change-photo"),
        $profileImgInput = document
        .getElementById("profile-img-input"),
        $error = document.getElementById("error");

    var currentProfileImg = "",
        profileImgDivW = getSizes($profileImgDiv).elW,
        NewImgNatWidth = 0,
        NewImgNatHeight = 0,
        NewImgNatRatio = 0,
        NewImgWidth = 0,
        NewImgHeight = 0,
        NewImgRatio = 0,
        xCut = 0,
        yCut = 0;

    makeSquared($profileImgDiv);

    $changePhoto.addEventListener("change", function() {
        currentProfileImg = $profileImg.src;
        showPreview(this, $profileImg);
        $profileImgInput.style.display = "flex";

    });


    function makeSquared(el) {
        var elW = el.clientWidth;
        el.style.height = elW + "px";
    }

    function showPreview(input, el) {
        var reader = new FileReader();
        reader.readAsDataURL(input.files[0]);
        if (input.files && input.files[0]) {
            reader.onload = function(e) {
                setTimeout(function() {
                    el.src = e.target.result;
                }, 300);

                var poll = setInterval(function() {
                    if (el.naturalWidth && el.src != currentProfileImg) {
                        clearInterval(poll);
                        setTimeout(function() {
                            $profileImg.style.opacity = "1";
                            // $('.c-avatar__edit-tab-wrapper').addClass("selected-img");
                            // $('.tabs li:first-child').removeClass("current");
                            // $('.tabs li:last-child').addClass("current");
                            // console.log('lOADER ');
                            $profileImgInput.style.display = "none";
                        }, 300);
                        $cropper.destroy();
                        $cropper.init();
                        $(".c-avatar__edit-tab-wrapper li:last-child").addClass("click");
                        $(".next-step-ava.disabled").removeClass("disabled");
                    }
                }, 100);
            };
        } else {
            return;
        }
    }


    function getSizes(el) {
        var elW = el.clientWidth,
            elH = el.clientHeight,
            elR = elW / elH;
        return {
            elW: elW,
            elH: elH,
            elR: elR
        };
    }

    function checkMinSizes(el) {
        if (getNatSizes(el).elW > 400 && getNatSizes(el).elH > 400) {
            return true;
        } else {
            return false;
        }
    }
}




$('.c-avatar__edit-tab-wrapper li').on('click', function() {
    capture($image);
});



function download() {
    var download = document.getElementById("downloadImg");

    var image = document.getElementById("user-img").toDataURL("image/png")
        .replace("image/png", "image/octet-stream");
    download.setAttribute("href", image);
    download.setAttribute("download", "image.png");
    $('.slider-nav').slick('unslick').slick(mainNavAvaSlickOptions);
}


$(".next-step-ava.disabled").on('click', function() {
    $(this).closest('.tab-content:first-child').removeClass("tab-active current");
    $(this).closest('.c-avatar__edit-tab-wrapper').find('.result').addClass("tab-active current");
    $(this).closest('.c-avatar__edit-tab-wrapper').find('.tabs li:first-child').removeClass("current");
    $(this).closest('.c-avatar__edit-tab-wrapper').find('.tabs li:last-child').addClass("current");
});